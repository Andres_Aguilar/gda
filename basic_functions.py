#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  basic_functions.py
#
#  Copyright 2017 Andres Aguilar <andresyoshimar@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
from __future__ import print_function
import shlex
import pandas as pd

from os import path
from Bio import SeqIO
from sys import stderr
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq, reverse_complement, translate
from basic_conf import BLAST_COLUMNS, PHYML_PATH, GFF_COLUMNS
from Bio.Blast.Applications import NcbiblastnCommandline as blastcmd

try:
    # Python2.7
    from multiprocessing import Popen
except ImportError as e:
    # Python3+
    from subprocess import Popen

# #############################################################################
# Global variables
VERSION = (0, 0, 1)
__author__ = "Andres Aguilar"
__mail__ = "andresyoshimar@gmail.com"
__date__ = "09/03/2017"
__version__ = ".".join(str(x) for x in VERSION)


# #############################################################################
# Functions
def translate_sequences(reads, reverse=True, frame=1):
    """ Function: translate_sequences
    Translate sequences stored into a SeqRecord iterable object

    params
    ------
     reads   : SeqRecord iterable object
     reverse : perform a reverse complement
     frame   : frame to translate sequences

    return
    ------
     list of SeqRecord object
    """
    try:
        assert isinstance(reads, list)
        assert isinstance(reads[0], SeqRecord)
    except:
        print("ERROR: reads must be a list of SeqRecord objects", file=stderr)
        return None

    try:
        assert 6 >= frame >= 1
    except AssertionError:
        print("ERROR: frame must be in 1-6 range", file=stderr)
        return None

    reads_t = list()
    for read in reads:
        # put sequence in X frame
        seq = str(read.seq)
        if frame == 2:
            seq = seq[1:]
        elif frame == 3:
            seq = seq[2:]
        elif frame == 4:
            seq = seq[::-1]
        elif frame == 5:
            seq = seq[::-1]
            seq = seq[1:]
        elif frame == 6:
            seq = seq[::-1]
            seq = seq[2:]

        if reverse:
            seq_r = reverse_complement(seq)
        else:
            seq_r = str(seq)
        seq_t = translate(seq_r)
        reads_t.append(SeqRecord(Seq(seq_t), id=read.id, name=read.name,
                                 description=read.description))
    return reads_t


def reads2df(reads):
    """ Function: reads2df
    Convert a list of SeqRecords into a pandas DataFrame
    """
    names = list()
    seqs = list()
    for read in reads:
        name = read.id
        names.append(name)
        seqs.append(str(read.seq))
    return pd.DataFrame({"Name": names, "Seq": seqs})


def df2reads(dataframe, id_col="Name", seq_col="Seq"):
    """ Function: df2reads
    Convert pandas DataFrame into a SeqRecord list
    """
    result = list()
    for i in dataframe.index:
        name = dataframe[id_col][i]
        seq = dataframe[seq_col][i]
        result.append(SeqRecord(Seq(seq, IUPAC.ambiguous_dna),
                                name=name, id=name, description=""))
    return result


# #############################################################################
# Readers
def read_fasta(path_to_file):
    """ Function: read_fasta
    Read a fasta formated file

    'Not recommended for large files'
    """
    if path.exists(path_to_file) and path.isfile(path_to_file):
        return SeqIO.parse(path_to_file, "fasta")
    else:
        return None


def read_blast_6(path_to_file, wseq=False):
    """ Function: read_blast_6

    Read a tsv file corresponding to a blast output (outfmt 6 format)

    params
    -------
     path_to_file : full path to blast output file
     wseq         : boolean flag to specify if file contains a column
                    with the sequence

    return
    -------
     Pandas DataFrame
    """
    if path.exists(path_to_file) and path.isfile(path_to_file):
        if wseq:
            return pd.read_table(path_to_file, header=None,
                                 names=BLAST_COLUMNS+["Seq"])
        else:
            return pd.read_table(path_to_file, header=None,
                                 names=BLAST_COLUMNS)
    else:
        return None


def read_gff(path_to_file, comment="#"):
    """ Function: read_gff """
    if path.exists(path_to_file) and path.isfile(path_to_file):
        return pd.read_table(path_to_file, header=None, names=GFF_COLUMNS,
                             comment=comment, engine='c')
    else:
        return None


# #############################################################################
# Writters
def write_fasta(reads, path_to_fasta):
    """ Function: write_fasta
    Writes a iterable of SeqRecords into a file
    """
    return SeqIO.write(reads, path_to_fasta, "fasta")


def write_gff(table, path_to_file, mode="w"):
    """ Function: write_gff """
    return table.to_csv(path_to_file, header=False, index=False, sep="\t",
                        mode=mode)


# #############################################################################
# Runners
def run_blastn(db="", query="", out_file="", wseq=False):
    """ Function: run_blastn

    Run blastn 2.2.29+

    params
    ------
     db       : a valid blast database
     query    : fasta file to use as a query
     out_file : Path to output file (outfmt 6) Default: blast.out6.tsv

    Make a blast database
    -------
     makeblastdb -in fasta.fna -input_type fasta -dbtype nucl -out DB_Name
    """
    out_file = "blast.out6.tsv" if out_file == "" else out_file
    fmt = "6" if not wseq else "\"6 std qseq\""
    # if query == "" else query
    if query == "" or db == "":
        raise Exception("ERROR: db and query files are required!")

    run_blast = blastcmd(cmd="blastn", out=out_file, db=db,
                         outfmt=fmt,
                         query=query, num_threads=20, evalue=0.1,
                         word_size=11)
    std, err = run_blast()

    if err != '':
        raise Exception("ERROR: blast ends with errors - " + err)

    return out_file


def run_phyml(file_list, type="nt", bootstrap=1000):
    """ Function: run_phyml

    Run phyml with multiple files
    """
    if not (path.exists(PHYML_PATH) and path.isfile(PHYML_PATH)):
        raise Exception("ERROR: PHYML_PATH is not configured!")

    command_line = "{0} -i {1} -d {2} -b {3}"
    process = list()
    for infile in file_list:
        args = shlex.split(command_line.format(PHYML_PATH, infile,
                                               type, str(bootstrap)))
        process.append(Popen(args))
    for p in process:
        try:
            p.wait()  # wait process
            p.terminate()  # Close process
        except OSError as e:
            print("Exception: ", e)
