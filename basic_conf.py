#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  basic_conf.py
#
#  Copyright 2017 Andres Aguilar <andresyoshimar@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

from os import path

VERSION = (0, 0, 1)
__autor__ = "Andres Aguilar"
__mail__ = "andresyoshimar@gmail.com"
__date__ = "09/03/2017"
__version__ = ".".join(str(x) for x in VERSION)

# #############################################################################
GFF_COLUMNS = ["Chr", "Source", "Feature", "Start", "End", "Score",
               "Strand", "Frame", "Attribute"]
BLAST_COLUMNS = ["query_id", "target_id", "identity", "Len", "mismatch",
                 "gap_open", "q_start", "q_end", "t_start", "t_end",
                 "evalue", "bit_score"]

# #############################################################################
# Other user configurations (paths, files, etc)

# Scripts
PHYML_PATH = ""

# Files



